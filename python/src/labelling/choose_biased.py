import numpy


def choose_biased(data_frame, group_label="Rating"):
    probabilities = get_probabilities(data_frame, group_label)

    index = numpy.random.choice(data_frame.index, 1, p=probabilities)[0]
    return data_frame.loc[index].to_dict(), index


def get_probabilities(data_frame, group_label):
    n_per_group = data_frame[group_label].value_counts()
    n_groups = n_per_group.size

    probabilities = n_per_group[data_frame[group_label]]
    probabilities.index = data_frame.index

    return 1 / (probabilities * n_groups)
