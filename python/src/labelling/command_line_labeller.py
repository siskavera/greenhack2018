import pandas

from src.labelling.query_yes_no import query_yes_no
from src.labelling.query_string import query_string
from src.labelling.choose_biased import choose_biased

from src.analysis.get_reviews import read_reviews_from_file

RAW_FILENAME = "reviews_Electronics_5.json.gz"
LABELLED_FILENAME = "labelled_Electronics.csv"
RATING_COLUMN = "review_rating"
REVIEW_COLUMN = "review_text"

# RAW_FILENAME = "Amazon_Unlocked_Mobile.csv"
# LABELLED_FILENAME = "labelled_Amazon_Unlocked_Mobile.csv"
# RATING_COLUMN = "Rating"
# REVIEW_COLUMN = "Reviews"

MAX_TO_LABEL = 50


if __name__ == "__main__":
    raw_data = read_reviews_from_file(RAW_FILENAME, n_max=10000)
    # raw_data = pandas.read_csv(RAW_FILENAME)
    labelled_data = pandas.read_csv(LABELLED_FILENAME)
    unlabelled_data = raw_data[~raw_data.index.isin(labelled_data["ID"])]

    try:
        for i in range(MAX_TO_LABEL):
            review, index = choose_biased(unlabelled_data, group_label=RATING_COLUMN)
            worked_for = ""
            arrived_broken = False

            print(review[REVIEW_COLUMN])
            broken = query_yes_no("Is this item broken?")
            if broken:
                arrived_broken = query_yes_no("Did it arrive broken?")
                if not arrived_broken:
                    worked_for = query_string("After how much time did it break?")
            else:
                worked_for = query_string("What minimum time did it work for?")
            print()

            review["ID"] = index
            review["Broken"] = broken
            review["ArrivedBroken"] = arrived_broken
            review["BrokeAfter"] = worked_for

            labelled_data = labelled_data.append(review, ignore_index=True)
            unlabelled_data = unlabelled_data.drop(index)

        print("Congratulations, you labelled {} new reviews!".format(MAX_TO_LABEL))

    except KeyboardInterrupt:
        print("Labelling paused, writing results to file")

    finally:
        labelled_data.to_csv(LABELLED_FILENAME, index=False)
