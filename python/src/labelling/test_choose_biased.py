import numpy
import pandas

from .choose_biased import get_probabilities, choose_biased


def test_get_probabilities():
    # given
    data_frame = pandas.DataFrame({"group": [1, 2, 3, 4, 5, 1, 2, 1]})
    expected_probabilities = pandas.Series(
        [1 / 15, 1 / 10, 1 / 5, 1 / 5, 1 / 5, 1 / 15, 1 / 10, 1 / 15]
    )
    expected_probabilities.name = "group"

    # when
    probabilities = get_probabilities(data_frame, "group")

    # then
    numpy.testing.assert_almost_equal(sum(probabilities), 1)
    pandas.testing.assert_series_equal(probabilities, expected_probabilities)


def test_choose_biased():
    # given
    data_frame = pandas.DataFrame(
        {"Index": [3, 5, 7], "Rating": [1, 2, 3], "Text": ["one", "two", "three"]}
    )
    data_frame.index = data_frame["Index"]

    chosen_record, index = choose_biased(data_frame)

    assert isinstance(chosen_record, dict)
    assert all([x == y for x, y, in zip(chosen_record.keys(), data_frame.columns)])

    assert isinstance(index, numpy.int64)
    assert index == chosen_record["Index"]
