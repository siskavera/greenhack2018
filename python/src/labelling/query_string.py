def query_string(question):
    """Ask a question via raw_input() and return their answer.

    "question" is a string that is presented to the user.

    The "answer" return value is an arbitrary string
    """

    print(question)
    answer = input()
    return answer
