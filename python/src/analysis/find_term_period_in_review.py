from word2number import w2n
import spacy

NLP = spacy.load("en_core_web_sm")


def find_token(doc, word):
    for token in doc:
        if word in token.text:
            return token
    return None


def find_first_number(doc):
    for token in doc:
        if token.pos_ == "NUM":
            return w2n.word_to_num(token.text)
    return None


def find_term_period_in_review(review_text, time_period, term_lemma):
    doc = NLP(review_text)
    n_period = None
    sentence = None

    for token in doc:
        if token.lemma_ == term_lemma:
            time_period_token = find_token(token.subtree, time_period)
            if time_period_token:
                sentence = token.sent.text
                n_period = find_first_number(time_period_token.children)
                if not n_period:
                    n_period = 1

    return n_period, sentence
