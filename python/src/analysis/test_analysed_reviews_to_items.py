# pylint: disable=redefined-outer-name

import pandas
import pytest

from src.analysis.analysed_reviews_to_items import generate_item_record
from src.analysis.analysed_reviews_to_items import analysed_reviews_to_items_list
from src.test_helpers.test_data import generate_empty_product_record


@pytest.fixture
def analyzed_reviews():
    return pandas.DataFrame(
        {
            "review_text": [
                "nothing to see here",
                "worked for 2 months",
                "It worked for 1 year. It then broke after just 13 months",
            ],
            "lifespan": [None, 2, 6],
            "asin": [str(x) for x in [1, 1, 2]],
            "review_rating": [2.3, 2.3, 1.2],
        }
    )


def test_reviews_to_items_list(analyzed_reviews):
    expected_review_list = [
        {"asin": "1", "lifespan": 2, "rating": 2.3},
        {"asin": "2", "lifespan": 6, "rating": 1.2},
    ]
    review_dict = analysed_reviews_to_items_list(analyzed_reviews)

    assert review_dict == expected_review_list


def test_generate_record_non_unique(analyzed_reviews):
    with pytest.raises(RuntimeError) as err:
        generate_item_record(analyzed_reviews, "whatever")

    error_message = str(err.value)
    assert (
        error_message
        == "Trying to generate record with reviews from more than one product"
    )


def test_generate_record(analyzed_reviews):
    asin = "1"
    expected_record = {"asin": asin, "lifespan": 2, "rating": 2.3}

    record = generate_item_record(analyzed_reviews[:2], asin)
    assert record == expected_record


def test_generate_record_empty():
    asin = "1"
    expected_record = generate_empty_product_record(asin)

    record = generate_item_record(pandas.DataFrame([]), asin)
    assert record == expected_record
