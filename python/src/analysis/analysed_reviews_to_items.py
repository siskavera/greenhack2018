import pandas


def analysed_reviews_to_items_list(reviews: pandas.DataFrame):
    unique_asins = reviews["asin"].unique()
    items_list = []

    for asin in unique_asins:
        item_reviews = reviews[reviews.asin == asin]

        record = generate_item_record(item_reviews, asin)
        items_list.append(record)

    return items_list


def generate_item_record(item_reviews: pandas.DataFrame, asin: str):
    if item_reviews.empty:
        return {"lifespan": None, "rating": None, "asin": asin}

    if len(set(item_reviews["asin"])) != 1:
        raise RuntimeError(
            "Trying to generate record with reviews from more than one product"
        )

    mean_lifespan = item_reviews["lifespan"].mean(skipna=True)
    mean_rating = item_reviews["review_rating"].mean()

    return {"lifespan": mean_lifespan, "rating": mean_rating, "asin": asin}
