# pylint: disable=redefined-outer-name
# pylint: disable=no-self-use
import pytest
import pandas

from src.analysis import get_lifespans


@pytest.fixture
def reviews():
    return pandas.DataFrame(
        {
            "review_text": [
                "nothing to see here",
                "worked for 2 months",
                "It worked for 1 year. It then broke after just 13 months",
            ]
        }
    )


@pytest.mark.parametrize(
    "time_period,n_period,expected_lifespan",
    [
        ("day", 1, 1 / 30),
        ("month", 2, 2),
        ("week", 3, 3 * 7 / 30),
        ("year", 4, 4 * 365 / 30),
        ("day", 5, 5 / 30),
    ],
)
def test_convert_time_period_to_months(time_period, n_period, expected_lifespan):
    received_lifespan = get_lifespans.convert_time_period_to_months(
        n_period, time_period
    )

    assert received_lifespan == expected_lifespan


def test_get_lifespans(reviews):
    get_lifespans.expand_reviews_with_lifespans(reviews)
    expected_breakdowns = pandas.DataFrame(
        {
            "review_text": [
                "nothing to see here",
                "worked for 2 months",
                "It worked for 1 year. It then broke after just 13 months",
            ],
            "lifespan": [None, 2, (13 + 365 / 30) / 2],
        }
    )
    pandas.testing.assert_frame_equal(reviews, expected_breakdowns)
