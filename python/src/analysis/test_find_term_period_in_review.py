# pylint: disable=redefined-outer-name
# pylint: disable=no-self-use
import pytest
import spacy

from src.analysis.find_term_period_in_review import find_term_period_in_review
from src.analysis.find_term_period_in_review import find_token, find_first_number

NLP = spacy.load("en_core_web_sm")


@pytest.fixture
def time_period():
    return "month"


@pytest.fixture
def term():
    return "break"


class TestFindToken:
    def test_not_found(self):
        doc = NLP("There are no fruits here.")
        assert find_token(doc, "apple") is None

    def test_found(self):
        doc = NLP("There is an apple here.")
        assert find_token(doc, "apple") == doc[3]


class TestFindFirstNumber:
    def test_not_found(self):
        doc = NLP("There is no number in here")
        assert find_first_number(doc) is None

    def test_single(self):
        doc = NLP("There is one number in here")
        assert find_first_number(doc) == 1

    def test_multiple(self):
        doc = NLP("There is one number in here and two and 100")
        assert find_first_number(doc) == 1

    def test_numeric(self):
        doc = NLP("There is 1 number in here")
        assert find_first_number(doc) == 1


class TestFindTermPeriodInReview:
    def test_no_result(self, time_period, term):
        review = "There is nothing in here"
        period, words = find_term_period_in_review(review, time_period, term)

        assert period is None
        assert words is None

    def test_partial_match(self, time_period, term):
        review = "after 1 month it went away"
        period, words = find_term_period_in_review(review, time_period, term)

        assert period is None
        assert words is None

    def test_match(self, time_period, term):
        review = "It broke after 10 months this awful, awful device. I hate it."
        period, sentence = find_term_period_in_review(review, time_period, term)

        assert period == 10
        assert sentence == "It broke after 10 months this awful, awful device."

    def test_match_number_as_text(self, time_period, term):
        review = "It broke after ten months this awful, awful device. I hate it."
        period, sentence = find_term_period_in_review(review, time_period, term)

        assert period == 10
        assert sentence == "It broke after ten months this awful, awful device."

    def test_a(self, time_period, term):
        review = "It broke after a month. That is not acceptable."
        period, sentence = find_term_period_in_review(review, time_period, term)

        assert period == 1
        assert sentence == "It broke after a month."
