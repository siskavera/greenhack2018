import numpy

from src.analysis.find_term_period_in_review import find_term_period_in_review


TIME_PERIODS_IN_MONTHS = {"day": 1 / 30, "week": 7 / 30, "month": 1, "year": 365 / 30}
DEFAULT_TERMS = ["break", "work", "fail"]


def convert_time_period_to_months(n_period, time_period):
    n_period_float = float(n_period)

    time_period_in_months = TIME_PERIODS_IN_MONTHS[time_period]
    return n_period_float * time_period_in_months


def expand_reviews_with_lifespans(reviews, terms=None):
    if terms is None:
        terms = DEFAULT_TERMS.copy()

    lifespans = []
    for _, row in reviews.iterrows():
        review_lifespans = []

        for term in terms:
            for time_period in TIME_PERIODS_IN_MONTHS:
                n_period, _ = find_term_period_in_review(
                    row["review_text"], time_period, term
                )
                if n_period:
                    lifespan = convert_time_period_to_months(n_period, time_period)
                    review_lifespans.append(lifespan)
        lifespans.append(numpy.mean(review_lifespans))

    reviews["lifespan"] = lifespans
