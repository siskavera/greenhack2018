import json
import gzip
import hashlib
import pandas

from external.amazon_reviews import parse_reviews


DB_FILE_COLUMN_MAPPING = {
    "review_text": "reviewText",
    "review_rating": "overall",
    "review_author": "reviewerID",
    "review_posted_date": "reviewTime",
}


def hash_row(row: pandas.Series):
    return hashlib.md5(str.encode(str(row))).hexdigest()


def hash_data_frame_columns(data_frame: pandas.DataFrame):
    return data_frame.apply(hash_row, axis=1)


def read_reviews_from_file(file_name, n_max=50000):
    i = 0

    data_dict = {}
    for record in gzip.open(file_name, "rb"):
        record_dict = json.loads(record)
        record_for_db = {}
        for db_column, file_column in DB_FILE_COLUMN_MAPPING.items():
            record_for_db[db_column] = record_dict[file_column]
        record_for_db["asin"] = record_dict["asin"]

        data_dict[i] = record_for_db
        i += 1
        if i > n_max:
            break

    reviews = pandas.DataFrame.from_dict(data_dict, orient="index")
    reviews["id"] = hash_data_frame_columns(reviews)
    return reviews


def scrape_reviews(asin):
    extracted_data = parse_reviews(asin, base_url="http://www.amazon.co.uk/dp/")
    extracted_data_dict = {}

    for column in DB_FILE_COLUMN_MAPPING:
        extracted_data_dict[column] = [
            review[column] for review in extracted_data["reviews"]
        ]

    reviews = pandas.DataFrame(extracted_data_dict)
    reviews["asin"] = asin
    reviews["id"] = hash_data_frame_columns(reviews)
    return reviews
