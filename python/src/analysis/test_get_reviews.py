from unittest.mock import patch
import pandas

from src.analysis import get_reviews

TEST_PARSED_REVIEWS = {
    "reviews": [
        {
            "review_text": "First test review",
            "review_rating": 4.5,
            "review_author": "JaneSmith",
            "review_posted_date": "06 May 2016",
        },
        {
            "review_text": "Second test review",
            "review_rating": 3.5,
            "review_author": "JohnSmith",
            "review_posted_date": "22 May 2017",
        },
    ]
}

EMPTY_PARSED_REVIEWS = {"reviews": []}


def test_hash_row():
    test_row = pandas.Series([1, "a", 1.2])
    expected_hash = "1eb27302bafffbcea37bf739654294a5"
    assert get_reviews.hash_row(test_row) == expected_hash


def test_hash_dataframe():
    test_df = pandas.DataFrame(
        {"x": [1, 2, 3], "y": ["a", "bb", "ccc"], "z": [1.2, 3.4, 5.678]}
    )
    expected_hashes = pandas.Series(
        [
            "5c8e4571204edeaaa002e150dfce6138",
            "02231a16792558f54c3153829bd16fe8",
            "af944c5bf1eaec7008d02d08334150a0",
        ]
    )

    hashes = get_reviews.hash_data_frame_columns(test_df)
    pandas.testing.assert_series_equal(hashes, expected_hashes)


@patch.object(get_reviews, "parse_reviews", return_value=TEST_PARSED_REVIEWS)
def test_scrape_reviews(_):
    asin = "test_asin"
    expected_scraped_reviews = pandas.DataFrame(
        {
            "review_text": ["First test review", "Second test review"],
            "review_rating": [4.5, 3.5],
            "review_author": ["JaneSmith", "JohnSmith"],
            "review_posted_date": ["06 May 2016", "22 May 2017"],
            "asin": ["test_asin", "test_asin"],
            "id": [
                "0d7b310240d82447b2915787cf9cdefc",
                "37692ffa3393ba39df1c4a1be2dcbd9e",
            ],
        }
    )

    scraped_reviews = get_reviews.scrape_reviews(asin)
    pandas.testing.assert_frame_equal(expected_scraped_reviews, scraped_reviews)


@patch.object(get_reviews, "parse_reviews", return_value=EMPTY_PARSED_REVIEWS)
def test_scrape_reviews_empty(_):
    expected_columns = {
        "id",
        "review_text",
        "review_rating",
        "review_author",
        "review_posted_date",
        "asin",
    }
    asin = "test_asin"

    scraped_reviews = get_reviews.scrape_reviews(asin)
    assert scraped_reviews.empty
    assert set(scraped_reviews.columns) == expected_columns
