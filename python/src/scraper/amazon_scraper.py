import logging
from typing import Iterable

from dataclasses import dataclass
from pandas import datetime
import requests


@dataclass
class AmazonReview:
    review_author: str
    review_text: str
    review_rating: float
    review_posted_date: datetime.timestamp
    asin: str


def get_review_data(amazon_url, page_cursor):
    amazon_page_url = "{}?pageNumber={}".format(amazon_url, page_cursor)
    try:
        response = requests.get(amazon_page_url)
        if response.ok:
            return response
        logging.error("Error while getting page: error code = %s", response.status_code)
        return {}
    except ConnectionError:
        logging.error("could not url:%s and page:%d", amazon_url, page_cursor)
    return None


def parse_page(_):
    test_reviews = [
        AmazonReview(
            "Fred Flinstone", "This is a great product", 2, "05 4, 2014", "ABCDEF"
        ),
        AmazonReview(
            "Wilma Flinstone",
            "This is a crap product but my husband likes it",
            2,
            "05 4, 2014",
            "ABCDEF",
        ),
    ]
    return 2, test_reviews


def get_reviews(asin, base_url) -> Iterable[AmazonReview]:
    amazon_url = base_url + asin
    current_page = 1
    while True:
        response = get_review_data(amazon_url, current_page)

        if not response:
            logging.error("Error while getting the page %d. Aborting...", current_page)
            return

        total_pages, reviews = parse_page(response)
        for review in reviews:
            yield review

        current_page += 1
        if current_page >= total_pages:
            break


if __name__ == "__main__":
    this_base_url = "https://www.amazon.com/product-reviews/"
    this_asin = "B01ETPUQ6E"
    for this_review in get_reviews(this_asin, this_base_url):
        print(this_review)
