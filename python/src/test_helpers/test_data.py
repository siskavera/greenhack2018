def generate_empty_product_record(asin):
    return {"asin": asin, "lifespan": None, "rating": None}
