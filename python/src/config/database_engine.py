LOCAL = {"url": "localhost", "port": "5433", "user": "postgres", "password": "postgres"}

ENGINE_ADDRESS = "postgresql://postgres:postgres@localhost:5433/life-spanner".format(
    url=LOCAL["url"], port=LOCAL["port"], user=LOCAL["user"], password=LOCAL["password"]
)
