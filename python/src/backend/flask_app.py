# pylint: disable=no-member
from flask import Flask, jsonify
from sqlalchemy.orm.exc import NoResultFound

from src.analysis.get_lifespans import expand_reviews_with_lifespans
from src.analysis.analysed_reviews_to_items import generate_item_record
from src.backend.database import DB, Product
from src.config.database_engine import ENGINE_ADDRESS
from src.analysis.get_reviews import scrape_reviews


def create_app(database_uri=ENGINE_ADDRESS):
    app = Flask(__name__)
    app.config["SQLALCHEMY_DATABASE_URI"] = database_uri
    DB.init_app(app)
    return app


APP = create_app()


def generate_record_from_asin(asin):
    reviews = scrape_reviews(asin)
    expand_reviews_with_lifespans(reviews)
    data = generate_item_record(reviews, asin)
    return data


@APP.route("/")
def index():
    return "Welcome to our Flask app"


@APP.route("/get_processed_data_by_asin/<asin>")
def get_processed_data_by_asin(asin):
    try:
        data = Product.query.filter_by(asin=asin).one().as_dict()
    except NoResultFound:
        data = generate_record_from_asin(asin)
        DB.session.add(Product(**data))
    return jsonify(data)


if __name__ == "__main__":
    APP.run(debug=True)
