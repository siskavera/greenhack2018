# pylint: disable=no-member
import unittest
from unittest.mock import patch

import json
from sqlalchemy.orm.exc import NoResultFound
import pytest

from src.backend.database import DB, Product, Review
import src.backend.flask_app as flask_app


class TestGetAsin(unittest.TestCase):
    SQLALCHEMY_DATABASE_URI = "sqlite://"

    def setUp(self) -> None:
        self.app = flask_app.create_app(self.SQLALCHEMY_DATABASE_URI)
        with self.app.app_context():
            DB.create_all()
            self.add_test_data()

    def tearDown(self) -> None:
        with self.app.app_context():
            DB.drop_all()

    def add_test_data(self):
        self.asin_in_db = "test_asin"

        with self.app.app_context():
            self.product_dict = {
                "asin": self.asin_in_db,
                "lifespan": 5.1,
                "n_reviews": 10,
                "n_lifespan_reviews": 2,
            }
            DB.session.add(Product(**self.product_dict))

            review_dicts = [
                {"id": 1, "asin": self.asin_in_db, "review_text": "Test review 1"},
                {"id": 2, "asin": self.asin_in_db, "review_text": "Test review 2"},
            ]
            for review_dict in review_dicts:
                DB.session.add(Review(**review_dict))

            DB.session.commit()

    def get_product_by_id(self, product_id):
        with self.app.app_context():
            return Product.query.filter_by(asin=product_id).one().as_dict()

    def test_get_processed_data_by_asin_existing(self):
        expected_record = {
            "asin": self.asin_in_db,
            "lifespan": 5.1,
            "n_lifespan_reviews": 2,
            "n_reviews": 10,
            "rating": None,
        }

        with self.app.app_context():
            received_record = json.loads(
                flask_app.get_processed_data_by_asin(self.asin_in_db).data
            )

            assert received_record == expected_record
            assert self.get_product_by_id(self.asin_in_db) == expected_record

    @patch.object(flask_app, "generate_record_from_asin")
    def test_get_processed_data_by_asin_new(self, mock_generate_record):
        test_id = "1"
        test_record = {
            "asin": test_id,
            "lifespan": 1.2,
            "n_lifespan_reviews": 1,
            "n_reviews": 2,
            "rating": 2.3,
        }
        mock_generate_record.return_value = test_record

        with self.app.app_context():
            with pytest.raises(NoResultFound):
                self.get_product_by_id(test_id)

            received_record = json.loads(flask_app.get_processed_data_by_asin(1).data)
            record_in_db = self.get_product_by_id(test_id)

            assert received_record == test_record
            assert record_in_db == test_record
