# pylint: disable=no-self-use
from unittest.mock import patch

from src.backend import flask_app
from src.test_helpers.test_data import generate_empty_product_record


@patch("src.analysis.get_reviews.parse_reviews")
class TestGenerateRecord:
    def test_results(self, mock_parsed_reviews):
        mock_parsed_reviews.return_value = {
            "reviews": [
                {
                    "review_text": "It worked for 3 months",
                    "review_rating": 2,
                    "review_author": "JohnSmith",
                    "review_posted_date": "01 Nov 1990",
                }
            ]
        }

        record = flask_app.generate_record_from_asin("test_asin")
        expected_record = {"asin": "test_asin", "lifespan": 3, "rating": 2}

        assert record == expected_record

    def test_no_results(self, mock_parsed_reviews):
        mock_parsed_reviews.return_value = {"reviews": []}
        asin = "will be mocked"
        expected_record = generate_empty_product_record(asin)

        record = flask_app.generate_record_from_asin(asin)

        assert record == expected_record
