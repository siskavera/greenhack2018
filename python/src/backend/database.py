# pylint: disable=too-few-public-methods,no-member
from flask_sqlalchemy import SQLAlchemy

DB = SQLAlchemy()


class Product(DB.Model):
    __tablename__ = "product"
    asin = DB.Column(DB.String, primary_key=True)
    lifespan = DB.Column(DB.Float)
    rating = DB.Column(DB.Float)
    n_reviews = DB.Column(DB.Integer)
    n_lifespan_reviews = DB.Column(DB.Integer)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Review(DB.Model):
    __tablename__ = "review"
    id = DB.Column(DB.String, primary_key=True)
    review_author = DB.Column(DB.String)
    review_text = DB.Column(DB.String)
    review_rating = DB.Column(DB.Float)
    review_posted_date = DB.Column(DB.String)
    lifespan = DB.Column(DB.Float)

    asin = DB.Column(DB.String, DB.ForeignKey("product.asin"))
    product = DB.relationship(Product)
