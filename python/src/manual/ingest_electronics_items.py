# pylint: disable=no-member
import logging

from src.analysis.get_lifespans import expand_reviews_with_lifespans
from src.analysis.get_reviews import read_reviews_from_file
from src.analysis.analysed_reviews_to_items import analysed_reviews_to_items_list
from src.backend.database import DB, Product, Review
from src.backend.flask_app import APP


def main():
    logging.info("Reading data")
    review_file_name = "../../data/reviews_Electronics_5.json.gz"
    reviews = read_reviews_from_file(review_file_name, n_max=5)

    logging.info("Extracting analyzed_reviews")
    expand_reviews_with_lifespans(reviews)

    logging.info("Writing results")
    items_list = analysed_reviews_to_items_list(reviews)
    with APP.app_context():
        for item in items_list:
            products = Product.query.filter_by(asin=item["asin"])
            if not products.first():
                DB.session.add(Product(**item))
            else:
                products.update(item)
        DB.session.commit()

        for _, row in reviews.iterrows():
            reviews = Review.query.filter_by(id=row["id"])
            if not reviews.first():
                DB.session.add(Review(**row))

        DB.session.commit()


if __name__ == "__main__":
    main()
