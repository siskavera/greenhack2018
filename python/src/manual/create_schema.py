from src.backend.database import DB
from src.backend.flask_app import APP

if __name__ == "__main__":
    with APP.app_context():
        DB.create_all()
