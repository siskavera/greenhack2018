# pylint: disable=no-member
from src.backend.database import DB, Product, Review
from src.backend.flask_app import APP

if __name__ == "__main__":
    with APP.app_context():
        PRODUCT_DICT = {
            "asin": "5",
            "lifespan_month": 1.5,
            "n_reviews": 10,
            "n_lifespan_reviews": 2,
        }
        DB.session.add(Product(**PRODUCT_DICT))

        REVIEW_DICTS = [
            {"asin": "5", "review_text": "First review"},
            {"asin": "5", "review_text": "Second review"},
        ]
        for review_dict in REVIEW_DICTS:
            DB.session.add(Review(**review_dict))

        DB.session.commit()
