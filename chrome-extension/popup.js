/**
 * Get the current URL.
 *
 * @param {function(string)} callback called when the URL of the current tab
 *   is found.
 */
function getCurrentTabUrl(callback) {
	// Query filter to be passed to chrome.tabs.query - see
	// https://developer.chrome.com/extensions/tabs#method-query
	const queryInfo = {
		active: true,
		currentWindow: true
	};

	//chrome.extension.getBackgroundPage().console.log('button was clicked!');

	chrome.tabs.query(queryInfo, (tabs) => {
		// chrome.tabs.query invokes the callback with a list of tabs that match the
		// query. When the popup is opened, there is certainly a window and at least
		// one tab, so we can safely assume that |tabs| is a non-empty array.
		// A window can only have one active tab at a time, so the array consists of
		// exactly one tab.
		const tab = tabs[0];

		// A tab is a plain object that provides information about the tab.
		// See https://developer.chrome.com/extensions/tabs#type-Tab
		const url = tab.url;

		// tab.url is only available if the "activeTab" permission is declared.
		// If you want to see the URL of other tabs (e.g. after removing active:true
		// from |queryInfo|), then the "tabs" permission is required to see their
		// "url" properties.
		console.assert(typeof url == 'string', 'tab.url should be a string');

		callback(url);
	});

	// Most methods of the Chrome extension APIs are asynchronous. This means that
	// you CANNOT do something like this:
	//
	// var url;
	// chrome.tabs.query(queryInfo, (tabs) => {
	//   url = tabs[0].url;
	// });
	// alert(url); // Shows "undefined", because chrome.tabs.query is async.
}

function displayProductInformation(product) {
	const avgLifespan = product.lifespan;
	const avgRating = product.rating;

	const productInfoLabel = document.getElementById('productInfo');
	productInfoLabel.innerHTML = `
		Average lifespan: <b>${avgLifespan}</b> months<br>
		Average rating: <b>${avgRating}/5</b>`;
}

document.addEventListener('DOMContentLoaded', () => {
	getCurrentTabUrl((url) => {
		const productInfoLabel = document.getElementById('productInfo');
		const statusLabel = document.getElementById('statusLabel');
		let ASIN = null;

		const regex = RegExp("https://www.amazon.co.uk/([\\w-]+/)?(dp|gp/product)/(\\w+/)?(\\w{10})");
		m = url.match(regex);
		if (m) {
			ASIN = m[4];
		} else {
			productInfoLabel.innerHTML = 'Sorry, Lifespanner only works with Amazon for now.';
		}

		if (ASIN) {
            fetch('http://127.0.0.1:5000/get_processed_data_by_asin/' + ASIN)
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log('Looks like there was a problem. Status Code: ' +
                                response.status);
                            statusLabel.textContent = response.status;
                        }

                        // Examine the text in the response
                        response.json().then(function(product) {
                            if (product) {
                                statusLabel.textContent = "We have some useful information about this product.";
                                displayProductInformation(product);
                            } else {
                                productInfoLabel.textContent = 'Sorry, but we don\'t have this product in our database yet.'
                            }
                        });
                    }
                )
                .catch(function(err) {
                    console.log('Fetch Error :-S', err);
                });
        }
	});
});
